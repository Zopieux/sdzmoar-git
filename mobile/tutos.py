#!/usr/bin/env python
# -*- coding: utf8 -*-

###
# Copyright (c) 2011, Alexandre `Zopieux` Macabies
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import re

from annoying.decorators import render_to

from sdzmoar.common.utils import *
from templatetags.utils import thsep
from settings import BIGNUMBERS_SCIENTIFIC_FORMAT

regex_catlink = re.compile(ur'tutoriel-(?P<mode>[12])-(?P<id>\d+)', re.I)
regex_catcount = re.compile(ur'\((?P<count>\d+)\)$')
regex_tutolink = re.compile(ur'tutoriel-3-(?P<id>\d+)', re.I)
regex_difficulty = re.compile(ur'niveau_(?P<level>\d+)', re.I)

ttf_fields = (u'mois', u'jour', u'heure', u'minute')
difficulties_text = {1: u'facile', 2: u'modérée', 3: u'difficile'}

@render_to('mobile/tutos/official.html')
def list_official(request):
    # an almost-static page that includes the official tuto menu
    soup = get_soup_or_404('http://www.siteduzero.com/mentions.html', timeout=Times.month)
    items = soup.xpath('//div[@id="menu_cours"]/div/ul/li[@class="menu_section"][not(@style)]/div')

    categories = [{
        'name': clean_text(cat.find('h5/a').text),
        'tutos':
            [{
                'title': clean_text(tuto.text),
                'id': regex_tutolink.search(tuto.get('href')).group('id'),
            } for tuto in cat.findall('ul/li/a[1]')]
    } for cat in items]

    return {'categories': categories}

@render_to('mobile/tutos/hits.html')
def list_hits(request):
    page = get_page(request)
    soup = get_soup_or_404('http://www.siteduzero.com/tutoriel-48-p%i-les-tutoriels-preferes-des-zeros.html' % page, timeout=Times.day * 3)

    tutos = []
    for elem in soup.findall('//table[@class="liste_cat"]/tbody/tr'):
        tds = elem.findall('td')
        link = tds[0].find('p/a')
        tutos.append({
            'title': clean_text(link.text),
            'id': regex_tutolink.search(link.get('href')).group('id'),
            'fans': scientific_int(clean_int(tds[1].text)),
            'authors': [sdz_member(_) for _ in tds[2].findall('a')],
            'icon': tds[0].find('a/img').get('src'),
        })

    pager = SdzPager.fromHtml(soup.find('//table[@class="liste_cat"]'))
    return {'tutorials': tutos, 'pager': pager}

def _hierarchy(soup):
    pseudotree = soup.find('//h1').itersiblings(tag='a')
    tree = []
    for elem in pseudotree:
        try:
            mode, id = regex_catlink.search(elem.get('href')).groups()
        except (AttributeError, ValueError): # not a valid tree link (eg. tutos préférés)
            continue

        assert mode == '1'
        tree.append({
            'name': clean_text(elem.xpath('text()[2]')[0]),
            'id': id,
        })

    lastimg = soup.xpath('//h1/following-sibling::img')[-1]
    try:
        count = int(regex_catcount.search(lastimg.tail.strip()).group('count'))
    except AttributeError: # not available, typically in tuto listings
        count = None

    current = {
        'name': clean_text(lastimg.get('title')),
        'count': count,
    }
    return tree, current

def _categories(id):
    soup = get_soup_or_404('http://www.siteduzero.com/tutoriel-1-%i-cours.html' % id, timeout=Times.day)

    categories = []
    for elem in soup.findall('//div[@class="infobox bouton_tuto"]'):
        mode, id = regex_catlink.search(elem.find('span[@class="image_cat"]/a').get('href')).groups()
        categories.append({
            'id': id,
            'is_cat': mode == '1',
            'name': clean_text(elem.find('h3').text),
            'description': clean_text(elem.find('span[@class="fontsize_08 gris"]').text),
            'icon': elem.find('span[@class="image_cat"]/a/img').get('src'),
            'count': int(elem.find('span[@class="nb_tuto fontsize_09 gras gris"]/a').text.split()[0]), # "X Cours"
        })

    # build the path
    tree, current = _hierarchy(soup)
    return {'categories': categories, 'tree': tree, 'current': current}

@render_to('mobile/tutos/categories.html')
def categories(request, id):
    return _categories(int(id))

def _list_tutorials(id):
    soup = get_soup_or_404('http://www.siteduzero.com/tutoriel-2-%i-cours.html' % id, timeout=Times.day)

    tutorials = []
    for elem in soup.findall('//table[@class="liste_cat"]/tbody/tr'):
        is_big = elem.get('class') == 'sous_cat'
        tds = elem.findall('td')
        if tds[0].get('class') == 'pas_tutos':
            break

        views = clean_int(tds[3].text)
        if BIGNUMBERS_SCIENTIFIC_FORMAT:
            formated_views = scientific_int(views)
        else:
            formated_views = thsep(views)

        tutorials.append({
            'id': regex_tutolink.search(tds[1].find('a').get('href')).group('id'),
            'is_big': is_big,
            'title': clean_text(tds[1].find('a/strong' if is_big else 'a').text),
            'authors': [sdz_member(_) for _ in tds[2].findall('a')],
            'views': formated_views,
            'date_validation': sdz_datetime(tds[4].text),
            'icon': tds[0].find('a/img').get('src'),
        })

    # build the path
    tree, current = _hierarchy(soup)
    # the current['count'] is None, we have to give the value from the number of rows we just got
    current['count'] = len(tutorials)

    return {'tutorials': tutorials, 'tree': tree, 'current': current}

@render_to('mobile/tutos/list_tutorials.html')
def list_tutorials(request, id):
    return _list_tutorials(int(id))

def _view(id):
    soup = get_soup_or_404('http://www.siteduzero.com/tutoriel-3-%s.html' % id, timeout=Times.week)

    def _licence():
        try:
            elem = soup.xpath(u'//div[@class="cadre_auteur_date"]//strong[text()="Licence"]')[0]
            next = elem.getnext()
            if next.tag == 'a':
                href = next.get('href')
                if href.startswith("http://creativecommons.org/licenses"):
                    return u"Creative Commons " + href[7:].split('/')[2].upper()
            else:
                if u"copie non autorisée" in elem.tail.lower():
                    return u"Copyright"
        except Exception:
            return None
        return None

    def _difficulty():
        try:
            diff = regex_difficulty.search(soup.find('//div[@class="cadre_auteur_date"]//span[@class="level_indication"]/img').get('src')).group('level')
            diff = int(diff)
            return {'code': diff, 'text': difficulties_text[diff]}
        except (AttributeError, ValueError):
            return None

    def _time_to_finish():
        try:
            elem = soup.xpath(u'//div[@class="cadre_auteur_date"]//strong[text()="Temps d\'étude estimé"]')[0]
            formated = clean_text(elem.tail).strip(':').strip()
            tt = []
            for lookfor in ttf_fields:
                v = re.search(ur'(\d+)[\s\n]*' + re.escape(lookfor), formated)
                if v is None:
                    tt.append(0)
                else:
                    tt.append(int(v.groups()[0]))
            return u', '.join()
        except Exception, err:
            return None

    def _last_release():
        try:
           elem = soup.xpath(u'//div[@class="cadre_auteur_date"]//strong[text()="Modifié"]')[0]
           return sdz_datetime(clean_text(elem.tail).strip(':'))
        except Exception:
            return None

    is_big = soup.find('//div[@id="btuto_intro"]') is not None
    if not is_big:
        assert soup.find('//div[@id="chap_intro"]') is not None

    data = {
        'authors': map(sdz_member, soup.xpath(u'//div[@class="cadre_auteur_date"]//a[@class="auteur_tut"]')),
        'licence': _licence(),
        'difficulty': _difficulty(),
        'time_to_finish': _time_to_finish(),
        'last_release': _last_release(),
        'title': clean_text(soup.find('//h1').text),
        'is_big': is_big,
    }

    if is_big:
        data['introduction'] = zcode_parser(html_string(soup.find('//div[@id="btuto_intro"]')))
        data['conclusion'] = zcode_parser(html_string(soup.find('//div[@id="btuto_conclusion"]')))

        data['parts'] = [{
            'title': clean_text(part.find('h2').text.split(':', 1)[1]),
            'introduction': zcode_parser(html_string(part.find('div[1]'))),
            'conclusion': zcode_parser(html_string(part.find('div[2]'))),
            'chapters': [{
                'title': clean_text(chap.find('a').text.split(')', 1)[1]),
                'id': regex_tutolink.search(chap.find('a').get('href')).group('id'),
                'paragraphs': [{
                    'title': clean_text(para.text),
                    'anchor': para.get('href').split('#')[1],
                } for para in chap.findall('div[@class="droite_chapitre"]/ul/li/a')]
            } for chap in part.findall('ul/li')]
        } for part in soup.findall('//ul[@class="liste_parties_btut"]/li')]

    else:
        data['introduction'] = zcode_parser(html_string(soup.find('//div[@id="chap_intro"]')))
        data['conclusion'] = zcode_parser(html_string(soup.find('//div[@id="chap_conclusion"]')))

        para = []
        for elem in soup.findall('//div[@class="ss_part_texte"]'):
            link = elem.getprevious().find('a')
            para.append({
                'title': clean_text(link.text),
                'anchor': link.get('id'),
                'content': zcode_parser(html_string(elem)),
            })
        data['paragraphs'] = para

    return data

@render_to('mobile/tutos/view_tuto.html')
def view(request, id):
    return _view(int(id))
