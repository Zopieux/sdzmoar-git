#!/usr/bin/env python
# -*- coding: utf8 -*-

###
# Copyright (c) 2011, Alexandre `Zopieux` Macabies
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

from django.conf.urls.defaults import *

urlpatterns = patterns('',
    (r'^$', 'mobile.index.index'),

    (r'^news/$', 'mobile.news.index'),
    (r'^news/(?P<news_id>\d+)/$', 'mobile.news.show'),
    (r'^news/(?P<news_id>\d+)/commentaires/$', 'mobile.news.show_comments'),

    url(r'^tutos/$', 'mobile.tutos.categories', kwargs={'id': 3}, name='mobile.tutos.index'),
    (r'^tutos/officiels/', 'mobile.tutos.list_official'),
    (r'^tutos/preferes/', 'mobile.tutos.list_hits'),
    (r'^tutos/categorie/(?P<id>\d+)/$', 'mobile.tutos.categories'),
    (r'^tutos/liste/(?P<id>\d+)/$', 'mobile.tutos.list_tutorials'),
    (r'^tutos/lire/(?P<id>\d+)/$', 'mobile.tutos.view'),

    (r'^forums/$', 'mobile.forums.index'),
    (r'^forums/categorie/(?P<id>\d+)/$', 'mobile.forums.category'),
    (r'^forums/(?P<id>\d+)/$', 'mobile.forums.topic'),

    (r'^membres/(?P<id>\d+)/$', 'mobile.members.profile'),
)
