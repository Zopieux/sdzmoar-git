#!/usr/bin/env python
# -*- coding: utf8 -*-

###
# Copyright (c) 2011, Alexandre `Zopieux` Macabies
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import re

from annoying.decorators import render_to

from sdzmoar.common.utils import *

regexp_basic_info = re.compile(r'<li><strong>(?P<name>[^:]*) : </strong>(?P<value>[^<]*)</li>')
regexp_activity = re.compile(r'<strong>(?P<name>[^:]*) : </strong>(?P<value>[^<]*)')

@render_to('mobile/members/profile.html')
def profile(request, id):
    soup = get_soup_or_404('http://www.siteduzero.com/membres-294-%s.html' % id, timeout=2 * Times.day)
    _igeneral, iaddrs, iactivity, iprofile, isignature, imylife = soup.findall('//div[@class="infobox"]')

    igeneral = _igeneral.xpath('descendant::strong')
    name = clean_text(igeneral[0].tail)
    group = clean_text(igeneral[1].tail)
    date_registered = sdz_datetime(igeneral[2].tail)
    date_lastseen = sdz_datetime(igeneral[3].tail)
    online = u'online' in unicode(_igeneral.xpath('div[2]//img')[0].get('src', u'offline'))
    signature = html_string(isignature.find('div'))
    mylife = html_string(imylife.find('div'))

    profile = []
    activity = []

    for item in iactivity.find('p').xpath('(a/strong)|strong'):
        activity.append({'name': clean_text(item.text).strip(': '), 'value': int(clean_text(item.tail))})

    for item in iprofile.xpath('descendant::li/strong'):
        next = item.getnext()
        if next is None:
            value = item.tail
        else:
            value = html_string(next)
        profile.append({'name': clean_text(item.text).strip(': '), 'value': value})

    return {
        'name': name,
        'date_registered': date_registered,
        'date_lastseen': date_lastseen,
        'group': group,
        'online': online,
        'activity': activity,
        'profile': profile,
        'mylife': mylife,
        'signature': signature,
    }
