#!/usr/bin/env python
# -*- coding: utf8 -*-

###
# Copyright (c) 2011, Alexandre `Zopieux` Macabies
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import re

from annoying.decorators import render_to

from sdzmoar.common.utils import *

regex_newsid = re.compile(r'news\-62\-(?P<id>\d+)', re.I)

def _index(page):
    soup = get_soup_or_404('http://www.siteduzero.com/news-73-p%i.html' % page, timeout=Times.hour / 2)

    news_list = []
    for entry in soup.findall('//table[@class="liste_cat"]/tbody/tr'):
        news_list.append({
            'logo': entry.xpath('td[1]/a/img')[0].get('src'),
            'title': clean_text(entry.xpath('td[2]/a[1]')[0].text),
            'id': regex_newsid.search(entry.xpath('td[2]/a[1]')[0].get('href')).group('id'),
            'short': zcode_parser(clean_text(entry.xpath('td[2]')[0].xpath('text()')[1]).rstrip(u'\u2026')),
            'category': clean_text(entry.xpath('td[3]')[0].text),
            'com_count': clean_int(entry.xpath('td[5]')[0].text),
            'date_published': sdz_datetime(entry.xpath('td[4]')[0].text),
        })

    pager = SdzPager.fromHtml(soup.find('//table[@class="liste_cat"]'))

    return {'news_list': news_list, 'pager': pager}

@render_to('mobile/news/list.html')
def index(request):
    page = get_page(request)
    return _index(page)

def _show(news_id):
    soup = get_soup_or_404('http://www.siteduzero.com/news-62-%s-p1.html' % news_id, timeout=Times.week)

    authors = []
    for elem in soup.xpath('//div[@class="cadre_info_news"]/div[2]/div/*')[1:]:
        if elem.tag != 'a':
            break
        authors.append(sdz_member(elem))

    return {
        'title': clean_text(soup.find('//h1').text),
        'authors': authors,
        'content': zcode_parser(html_string(soup.find('//div[@class="contenu_news"]'))),
        'news_id': news_id,
        'com_count': int(soup.find('//h2[@id="discussion"]').text.split()[0]),
    }

@render_to('mobile/news/view.html')
def show(request, news_id):
    return _show(news_id)

def _show_comments(news_id, page):
    soup = get_soup_or_404('http://www.siteduzero.com/news-62-%s-p%s-foo.html'% (news_id, page), timeout=5 * Times.minute)

    rawcomments = soup.findall('//table[@class="liste_messages"]/tbody/tr')
    comments = []
    tempmeta = {}
    for comment in rawcomments:
        if comment.get('class') == 'header_message':
            author = sdz_member(comment.find('td[1]//a'))
            if clean_text(comment.find('td[2]//span/a').tail).lower().startswith(u'commentaire supprimé'):
                comments.append({
                    'author': author,
                    'deleted': True,
                    'deletion_reason': clean_text(comment.find('td[2]//span/em').text),
                })
            else:
                tempmeta = {
                    'author': author,
                    'posted_on': sdz_datetime(clean_text(comment.find('td[2]//span/a').tail)[6:]), # Posté <date>
                }
        else:
            content = comment.find('td[2]//div[@class="message_txt"]')
            sig = content.find('div[@class="signature"]')
            if sig is not None:
                content.remove(sig)
            res = {'content': zcode_parser(html_string(content))}
            res.update(tempmeta)
            comments.append(res)

    pager = SdzPager.fromHtml(soup.find('//table[@class="liste_messages"]'))

    return {
        'news_id': news_id,
        'title': clean_text(soup.find('//h1').text),
        'pager': pager,
        'comment_list': comments
    }

@render_to('mobile/news/view_comments.html')
def show_comments(request, news_id):
    page = get_page(request)
    return _show_comments(news_id, page)
