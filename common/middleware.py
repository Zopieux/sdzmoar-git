#!/usr/bin/env python
# -*- coding: utf8 -*-

###
# Copyright (c) 2011, Alexandre `Zopieux` Macabies
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import re
from django.utils.encoding import force_unicode
from django.conf import settings
from django.views.generic.simple import direct_to_template

from htmlcompressor import HTMLCompressor
from common.utils import SdzDownError, SdzResponseError

RE_MULTISPACE = re.compile(r'\s{2,}')
RE_NEWLINE = re.compile(r'\n')
RE_DJANGOSTRIP = re.compile(r'>\s+<')

class SharpMinifyHTMLMiddleware(object):
    """
        Provides a simple (but severe!) middleware to compress HTML output on-the-fly.
        It does NOT keep important new lines, such as in <pre>, <code> or <textarea>.
    """
    def process_response(self, request, response):
        if 'text/html' in response['Content-Type'] and settings.COMPRESS_HTML and not settings.DEBUG:
            response.content = RE_DJANGOSTRIP.sub('> <', force_unicode(response.content.strip()))
            response.content = RE_MULTISPACE.sub(' ', response.content)
            response.content = RE_NEWLINE.sub('', response.content)
        return response

class MinifyHTMLMiddleware(object):
    """Provides a improved middleware to compress HTML output on-the-fly."""
    def process_response(self, request, response):
        if 'text/html' in response['Content-Type'] and settings.COMPRESS_HTML and not settings.DEBUG:
            compressor = HTMLCompressor()
            response.content = compressor.compress(force_unicode(response.content))
        return response

class ProcessExceptionMiddleware(object):
    def process_exception(self, request, exception):
        reason = None
        clsname = exception.__class__.__name__

        if clsname == 'SdzDownError':
            reason = u"le Site du Zéro semble être indisponible (down)."
        elif clsname == 'SdzResponseError':
            reason = u"la réponse du Site du Zéro à cette requête est innapropriée."

        response = direct_to_template(request, '500.html', {'reason': reason})
        response.status_code = 503 # temporary error! better for web scrawlers
        response['Retry-After'] = '3600' # web scrawlers again
        return response
