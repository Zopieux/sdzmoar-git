#!/usr/bin/env python
# -*- coding: utf8 -*-

###
# Copyright (c) 2011, Alexandre `Zopieux` Macabies
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

from settings import PAGER_SIBLING_COUNT, PAGE_GETPARAM, SDZ_DATETIME_FORMAT, SDZ_DATE_FORMAT, SDZ_TIME_FORMAT, SDZ_BASE_URL, SOCKET_TIMEOUT

import socket
socket.setdefaulttimeout(SOCKET_TIMEOUT)

import datetime
from lxml import etree, html
import re
import urllib
import math
from StringIO import StringIO
try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin

from django.core.urlresolvers import reverse
from django.http import Http404
from django.core.cache import cache

regex_smiley = re.compile(r'<img src="http://(?:www)?\.siteduzero\.com/Templates/images/smilies/[^"]+" alt="([^"]+)"(?: class="smilies")?\s*/>', re.I)
regex_memberid = re.compile(r'membres\-294\-(?P<id>\d+)', re.I)

regex_time = re.compile(r'(\d+):(\d+):(\d+)')
regex_timeago_unit = re.compile(r'(?P<amount>\d+)\s*(?P<unit>h|min|s)')
regex_timeago_hour = re.compile(r'(?P<hour>\d+)\s*h\s*(?P<min>\d+)')

class SdzError(Exception): pass
class SdzResponseError(SdzError): pass
class SdzDownError(SdzError): pass

class Times:
    minute = 60
    hour = 3600
    day = 86400
    week = 604800
    month = 18144000
    year = 31557600

_TIME_UNITS = {'h': Times.hour, 'min': Times.minute, 's': 1}

def zcode_parser(code):
    code, foo = regex_smiley.subn(r'\1', code)
    if code.endswith('<'):
        code = code[:-1]
    return code

def get_page(request, default=1):
    try:
        page = int(request.GET.get(PAGE_GETPARAM, default))
        if page < 1:
            raise Exception
        return page
    except Exception:
        return default

def clean_text(obj, keepnone=False):
    if isinstance(obj, basestring):
        return obj.strip()
    else:
        return None if keepnone else ''

def get_soup_or_404(url, **kwargs):
    opener = UrlOpener()
    try:
        soup = opener.getSoup(url, **kwargs)
        return soup
    except SdzResponseError:
        raise Http404

def get_int_or_404(value):
    try:
        return int(value)
    except ValueError:
        raise Http404

def sdz_datetime(datestr):
    datestr = datestr.lower().replace(u"le ", '').strip()
    try:
        # simple format for old dates
        for format in (SDZ_DATETIME_FORMAT, SDZ_DATE_FORMAT, SDZ_TIME_FORMAT):
            try:
                return datetime.datetime.strptime(datestr.encode('utf-8'), format)
            except ValueError:
                pass
        else:
            raise ValueError

    except ValueError:
        # ok, let's do the shit
        if u"hier" in datestr or u"aujourd'hui" in datestr:
            date = datetime.date.today()
            try:
                time = datetime.time(*map(int, regex_time.search(datestr).groups()))
            except AttributeError:
                time = datetime.time(0, 0, 0)
            if u"hier" in datestr:
                date -= datetime.timedelta(days = 1)
            return datetime.datetime.combine(date, time)
        elif u"il y a" in datestr:
            match_hour = regex_timeago_hour.search(datestr)
            match_unit = regex_timeago_unit.search(datestr)
            if match_hour:
                hours, mins = map(int, match_hour.groups())
                delta = 3600 * hours + 60 * mins
            elif match_unit:
                amount, unit = match_unit.groups()
                delta = int(amount) * _TIME_UNITS[unit]
            return datetime.datetime.now() - datetime.timedelta(seconds = delta)
        else:
            raise ValueError("Unknow datetime format '%s'." % datestr)

def sdz_member(htmllink):
    if htmllink is None:
        return None
    nickname = htmllink.text
    if nickname is None:
        span = htmllink.find('span')
        if span is not None:
            nickname = clean_text(span.text)
        else:
            nickname = '(inconnu)'
    try:
        return {
            'name': nickname,
            'id': regex_memberid.search(htmllink.get('href')).group('id'),
        }
    except AttributeError:
        return None

def absolute_url(href):
    return urljoin(SDZ_BASE_URL, href)

def html_string(html):
    # TODO: parse known links
    try: # black magic!
        html.make_links_absolute()
    except TypeError: # actually, you should have practise more with Severus Snape
        html.make_links_absolute(SDZ_BASE_URL)
    return etree.tostring(html, encoding=unicode).strip()

def clean_int(x):
    if isinstance(x, int):
        return x
    return int(x.strip().replace(u'\xa0', '').replace(u'&nbsp;', '').strip())

def scientific_int(value):
    powed = int(round(math.log(value, 10), 1))
    if powed <= 2:
        return str(value)
    else:
        part = int(math.ceil(float(value)/pow(10, powed)))
        return u'%i·10<sup>%i</sup>' % (part, powed)

class UrlOpener(urllib.FancyURLopener):
    version = 'SdZmoar'
    _cache_prefix = 'httpsdz_'

    def getPage(self, url, data=None, timeout=None, encoding='utf-8', clear_cache=False, cache_key='{url}'):
        key = UrlOpener._cache_prefix + cache_key.format(url=url)

        def download():
            try:
                page = self.open(url, data)
            except (socket.timeout, IOError):
                raise SdzDownError
            if page.code != 200:
                raise SdzResponseError
            rawdata = page.read()
            if encoding is None:
                return rawdata
            else:
                return rawdata.decode(encoding)

        if timeout is None:
            if clear_cache:
                cache.delete(key)
            rawdata = download()
        else:
            rawdata = cache.get(key)
            if rawdata is None:
                rawdata = download()
                cache.set(key, rawdata, timeout)

        return rawdata

    def getSoup(self, *args, **kwargs):
        return html.parse(StringIO(self.getPage(*args, **kwargs)))

    # TODO
    def getPaginatedSoup(self, url, *args, **kwargs):
        try:
            page = kwargs.pop('page')
            if not isinstance(page, int):
                raise TypeError("'page' argument is not integer in getPaginatedSoup")
        except KeyError:
            raise KeyError("'page' argument not foun in getPaginatedSoup")

        raise NotImplementedError

class SdzPager:
    def __init__(self):
        self.pages = 1
        self.current = 1
        self.reversed = False

    @property
    def has_next(self):
        return self.current < self.pages

    @property
    def has_previous(self):
        return self.current > 1

    @property
    def should_render_combo(self):
        return None in tuple(self)

    def _iter(self):
        sb = PAGER_SIBLING_COUNT # i'm lazy.
        i = 1
        while True:
            if i < sb or i > (self.pages - sb) or (i < (self.current + sb) and i > (self.current - sb)):
                yield i
            else:
                if i >= sb and i <= (self.current - sb):
                    i = self.current - sb
                elif i >= self.current + sb and i <= self.pages - sb:
                    i = self.pages - sb

                yield None

            i += 1
            if i > self.pages:
                break

    def __iter__(self):
        if self.reversed:
            l = list(self._iter())
            l.reverse()
            return iter(l)
        else:
            return self._iter()

    def range(self):
        if self.reversed:
            return xrange(self.pages, 0, -1)
        else:
            return xrange(1, self.pages + 1)

    def render(self, viewname, *args, **kwargs):
        """Convinient function, you should use a template chunck and iterate the SdzPager instead"""

        def linkspan(page):
            if page == self.current:
                return u'<span class="current-page">{text}</span>'
            else:
                return u'<a href="{href}">{text}</a>'

        def resolve(page):
            kwargs['page'] = page
            return reverse(viewname, args=args, kwargs=kwargs)

        output = []

        if self.has_previous:
            output.append(linkspan(self.current - 1).format(text=u'Précédente', href=resolve(self.current - 1)))

        for page in self:
            if page is None:
                output.append(u'…')
            else:
                output.append(linkspan(page).format(text=str(page), href=resolve(page)))

        if self.has_next:
            output.append(linkspan(self.current + 1).format(text=u'Suivante', href=resolve(self.current + 1)))

        return u' '.join(output)

    @classmethod
    def fromHtml(cls, html):
        pager = cls()
        pages = html.xpath('//thead/tr[1]/td[1]/*')
        try:
            pager.current = int(html.xpath('//thead/tr[1]/td[1]/span')[0].text)
        except (IndexError, AttributeError, ValueError):
            pass

        if not pages:
            return pager

        first = pages[0].text.strip().lower()
        last = pages[-1].text.strip().lower()

        # normal
        if first == u"précédente" or last == u"suivante":
            if last == u"suivante":
                pager.pages = int(pages[-2].text)
            else:
                pager.pages = int(last)
        elif first == u"suivante" or last == u"précédente":
            pager.reversed = True
            if first == u"suivante":
                pager.pages = int(pages[1].text)
            else:
                pager.pages = int(first)
        elif first != u"précédente" and last != u"suivante":
            intfirst = int(first)
            if intfirst == 1:
                pager.pages = int(last)
            else:
                pager.reversed = True
                pager.pages = intfirst
        else:
            pager.pages = 1

        return pager

__all__ = [
    'UrlOpener', 'SdzPager', 'SdzError', 'SdzDownError', 'SdzResponseError', 'Times', 'zcode_parser', 'get_page', 'sdz_datetime', 'sdz_member',
    'html_string', 'clean_int', 'clean_text', 'get_soup_or_404', 'scientific_int', 'get_int_or_404',
]