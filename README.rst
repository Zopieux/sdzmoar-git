=======
SdZmoar
=======

SdZmoar est un *fork* de `SdZ++ <https://github.com/ProgVal/SdZpp>`_ développé
par Zopieux.

Son but est de fournir, dans un premier temps, une interface très simplifiée
(et en lecture seule) du Site du Zéro, particulièrement accessible pour
les mobiles.

Il est possible de consulter :
 - les news et leurs commentaires ;
 - les forums ;
 - l'ensemble des tutoriels et articles.

SdZmoar ne permet pas, pour le moment, la navigation sur les sous-sites
tels que ``sciences.siteduzero.com``.

Utilisation
===========
SdZmoar est en fonctionnement à l'adresse http://sdzmoar.tk/, vous pouvez
y accéder librement.

Vous pouvez également déployer le logiciel sur votre serveur, les dépendances sont minimes :
 - Python 2.6 ou supérieur (mais *a priori* pas Python 3)
 - Django 1.3
 - `django-annoying <https://bitbucket.org/offline/django-annoying/overview>`_
   (devrait être utilisé par tout djangonaute qui se respecte !)

Pour déployer votre projet, reprenez le contenu de ``settings.py.sample``,
que vous renommez en ``settings.py``. Changez-y avant tout la ``SECRET_KEY`` et
les ``ADMINS`` ; le reste devrait convenir. Vous pouvez notamment modifier
les paramètres se trouvant en bas du fichier.

Contributions
=============
Vous êtes encouragé à signaler tout problème dans `l'interface dédiée <issues>`_.
Les suggestions sont également appréciées. N'hésitez pas à soumettre vos
*patchs* si vous avez des idées ou des correctifs à apporter.

TODO
====
Voici la liste, sans classement, des choses qu'il reste à implémenter.

- Cache intelligent concernant les pages ; actuellement le cache garde en mémoire,
  pendant la durée indiquée dans getSoup, chaque URL de manière indépendante.
  Il faudrait, pour les URL paginées, que le cache s'invalide pour toutes les
  autres pages lorsque l'une des pages a timeouté, afin d'éviter des incohérences
  entre les différentes pages d'une même section (doublons/oublis)
- Commentaires des tutoriels
- Tutoriels en rédaction/en bêta (nécéssite authentification)
- News en rédaction
- Sous-sites (sciences)
- Création d'un certain nombre d'options pour alléger ou fournir plus d'options
  à l'utilisateur (stockées en session : cookie), eg. l'affichage des images
- Modifier le comportement de ``make_absolute_links`` pour parser les liens
  qui peuvent être visités avec SdZmoar (eg. les profiles ou références à
  d'autres turoriels).

Licence
=======
SdZmoar est un projet libre. Il est distribué sous la *New BSD License*, en
accord avec la licence originale :
::

  Copyright (c) 2011, Alexandre `Zopieux` Macabies
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions, and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions, and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
  * Neither the name of the author of this software nor the name of
    contributors to this software may be used to endorse or promote products
    derived from this software without specific prior written consent.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.

